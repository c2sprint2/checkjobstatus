let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let Label = require('../js/components/common/Label'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('label testcases', function () {
  before('before creating label', function() {
    this.LabelComp = TestUtils.renderIntoDocument(<Label />);
    this.label = TestUtils.findRenderedDOMComponentWithTag(this.LabelComp, 'label');
  });

  it('Renderes Input tag with "pe-label" class', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.LabelComp);
    expect(renderedDOM.tagName).to.equal("LABEL");
    expect(renderedDOM.classList.length).to.equal(2);
    expect(renderedDOM.classList[0]).to.equal("pe-label");
    expect(renderedDOM.classList[1]).to.equal("pe-label--bold");
  });

  it('passing "text" property updates state and DOM element', function () {
    let LabelComp = TestUtils.renderIntoDocument(<Label text="sample text"/>);
    let label = TestUtils.findRenderedDOMComponentWithTag(LabelComp, 'label');
    //not sure how to check label text
    //expect(label.text()).to.equal("sample text");
  });

});