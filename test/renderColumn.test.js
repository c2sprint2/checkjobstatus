let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let RenderColumn = require('../js/components/checkJobStatus/RenderColumn'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let checkJobStatusAPI = require('../js/api/checkJobStatusAPI');
let chai = require('chai');
let expect = chai.expect;
 
describe('RenderColumn testcases', function () {
  before('before crating RenderColumn', function() {
    this.RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns} />); 
});
 
 it('Renderes component with div tag', function() {
   
     this.RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns}/>);
    let renderedDOM = ReactDOM.findDOMNode(this.RenderColumnComp);
    expect(renderedDOM.tagName).to.equal("DIV");
   
  });
 
   it('Renderes Div tag with "row" class', function() {
 
    this.RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns}/>);
    let renderedDOM = ReactDOM.findDOMNode(this.RenderColumnComp);
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("row");
 });
 
 it('should be rendered with several div', () => {
  let RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(RenderColumnComp, 'div');
  expect(divs.length).to.equal(5);
});

it('renders a composite component', () => {
  let RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns} />);
  expect(TestUtils.isCompositeComponent(RenderColumnComp)).to.equal(true);
});

it('does not render a react element', () => {
  let RenderColumnComp = TestUtils.renderIntoDocument(<RenderColumn cols={checkJobStatusAPI.columns} />);
  expect(TestUtils.isElement(RenderColumnComp)).to.equal(false);
});
 
});