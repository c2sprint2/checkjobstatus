let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let Table = require('../js/components/checkJobStatus/Table');
let RenderRow = require('../js/components/checkJobStatus/RenderRow'); 
let RenderColumn = require('../js/components/checkJobStatus/RenderColumn');
let checkJobStatusAPI = require('../js/api/checkJobStatusAPI'); 
let chai = require('chai');
let expect = chai.expect;

describe('Table testcases', function () {
	
  before('before creating Table', function() {
    this.TableComp = TestUtils.renderIntoDocument(<Table columns={checkJobStatusAPI.columns} 
             rows={checkJobStatusAPI.rows}  />);
   
  });

  it('renders a composite component', () => {
   this.TableComp = TestUtils.renderIntoDocument(<Table columns={checkJobStatusAPI.columns} 
             rows={checkJobStatusAPI.rows}  />);
  expect(TestUtils.isCompositeComponent(this.TableComp)).to.equal(true);
});

it('should be rendered with several div', () => {
  this.TableComp = TestUtils.renderIntoDocument(<Table columns={checkJobStatusAPI.columns} 
             rows={checkJobStatusAPI.rows}  />);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(this.TableComp , 'div');
  expect(divs.length).to.equal(32);
});

it('does not render a react element', () => {
  this.TableComp = TestUtils.renderIntoDocument(<Table columns={checkJobStatusAPI.columns} 
             rows={checkJobStatusAPI.rows}  />);
  expect(TestUtils.isElement(this.TableComp)).to.equal(false);
});


  });