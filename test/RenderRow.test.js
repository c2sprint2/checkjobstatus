let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let RenderRow = require('../js/components/checkJobStatus/RenderRow'); 
let checkJobStatusAPI=require('../js/api/checkJobStatusAPI');
let chai = require('chai');
let expect = chai.expect;

describe('RenderRow testcases', function () {
	debugger;
  before('before creating RenderRow', function() {
    this.RenderRowComp = TestUtils.renderIntoDocument(<RenderRow rows={checkJobStatusAPI.rows}/>);
  });

  it('should be rendered with several div', () => {
  let RenderRowComp = TestUtils.renderIntoDocument(<RenderRow rows={checkJobStatusAPI.rows}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(RenderRowComp, 'div');
  expect(divs.length).to.equal(21);
});

it('renders a composite component', () => {
  let RenderRowComp = TestUtils.renderIntoDocument(<RenderRow rows={checkJobStatusAPI.rows} />);
  expect(TestUtils.isCompositeComponent(RenderRowComp)).to.equal(true);
});

it('does not render a react element', () => {
  let RenderRowComp = TestUtils.renderIntoDocument(<RenderRow rows={checkJobStatusAPI.rows}/>);
  expect(TestUtils.isElement(RenderRowComp)).to.equal(false);
});
   



  });
