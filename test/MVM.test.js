let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let MVM = require('../js/components/MVM/MVM'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let TextBox = require('../js/components/common/TextBox');
let Label = require('../js/components/common/Label');
let Heading = require('../js/components/common/Heading');
let SelectBox = require('../js/components/common/SelectBox');
let TextArea = require('../js/components/common/TextArea');
let chai = require('chai');
let expect = chai.expect;

describe('textbox testcases', function () {
  before('before creating textbox', function() {
    this.MVM = TestUtils.renderIntoDocument(<MVM />);
});
  
it('Component Renders with the Correct DOM', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.MVM);
    expect(renderedDOM.tagName).to.equal("DIV");
    expect(renderedDOM.classList.length).to.equal(0);
    var children = renderedDOM.querySelectorAll('div');
    expect(children.length).to.equal(11);
});

it('should be rendered with several child TextBox', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(MVMComp, 'div');
  expect(divs.length).to.equal(12);
});

it('renders a composite component', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  expect(TestUtils.isCompositeComponent(MVMComp)).to.equal(true);
});

it('renders a  MVM component', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  expect(TestUtils.isCompositeComponentWithType (MVMComp,MVM)).to.equal(true);
});

it('renders a DOM component', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  //expect(TestUtils.isDOMComponent(MVMComp)).to.equal(true);
});

it('does not render a react element', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  expect(TestUtils.isElement(MVMComp)).to.equal(false);
});



it('should be rendered with several child TextBox', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, TextBox);
  expect(children.length).to.equal(4);
});

it('should be rendered with several child TextArea', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, TextArea);
  expect(children.length).to.equal(1);
});

it('should be rendered with several child Heading', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, Heading);
  expect(children.length).to.equal(1);
});

it('should be rendered with several child Label', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, Label);
  expect(children.length).to.equal(10);
});

it('should be rendered with several child SelectBox', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, SelectBox);
  expect(children.length).to.equal(5);
});

it('should render heading value as "Minimum Viable MetaData"', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, Heading);
  expect(children[0].props.value).to.equal("Minimum Viable MetaData");
});


it('should render heading with class as "MinimumViable"', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  //var children = TestUtils.findRenderedDOMComponentWithClass(MVMComp, 'MinimumViable');
  //expect(children.lenght).to.equal(1);
  //expect(children[0].tagName).to.equal(1);

});

it('should renders a div using shallow render', () => {
  var renderer = TestUtils.createRenderer();
  renderer.render(<MVM />);
  let result = renderer.getRenderOutput();
  expect(result.type).to.equal('div');
  const child = result.props.children[0];
  expect(child.type).to.equal('div');
  /*expect(child.props.children[0]).to.equal('div');
  .props.children.filter(component => component.type == TweetItem);*/
});


it('checking state after sequence fo events', () => {
  let MVMComp = TestUtils.renderIntoDocument(<MVM />);
  var children = TestUtils.scryRenderedComponentsWithType(MVMComp, TextBox);
  expect(children[0].props.id).to.equal('UUID');
  let input = TestUtils.findRenderedDOMComponentWithTag(children[0], 'input');
  TestUtils.Simulate.change(input, {target: {value: 'Hello, world',"id":"UUID"}});
  expect(MVMComp.state.UUID).to.equal("Hello, world");
  let input1 = TestUtils.findRenderedDOMComponentWithTag(children[1], 'input');
  TestUtils.Simulate.change(input1, {target: {value: 'value1',"id":"AssignmentTitle"}});
  expect(MVMComp.state.AssignmentTitle).to.equal("value1");
  let input2 = TestUtils.findRenderedDOMComponentWithTag(children[2], 'input');
  TestUtils.Simulate.change(input2, {target: {value: 'value2',"id":"QuestionName"}});
  expect(MVMComp.state.QuestionName).to.equal("value2");
  let input3 = TestUtils.findRenderedDOMComponentWithTag(children[3], 'input');
  TestUtils.Simulate.change(input3, {target: {value: 'value3',"id":"EnablingObejctive"}});
  expect(MVMComp.state.EnablingObejctive).to.equal("value3");
});
 
});

/**** using rewire
let rewire = require('rewire');
var ParentComponent = rewire('../js/components/MVM/MVM');
var TextBoxMock = React.createClass({
  render: function () {
    return <div className="textbox-mock-component" />;
  }
});
ParentComponent.__set__('js\\components\\common\\Label', TextBoxMock);
//////////////Label is not defined///
describe('ParentComponent', function() {
var textBoxType = React.renderToStaticMarkup(<TextBoxMock />);
it('should render with child', function() {
  var mvmComp = React.renderToStaticMarkup(<ParentComponent />);
  expect(mvmComp).to.contains(textBoxType);
});
it('should render without child', function() {
    var mvmComp = React.renderToStaticMarkup(<ParentComponent standalone={true} />);
    expect(mvmComp).to.not.contains(childType);
});

});*/

/**** using rewireModule
describe('ParentComponent', function() {

let rewire = require('rewire');
let rewireModule = require("./rewireModule");
var MVMComponent = rewire('../js/components/MVM/MVM');
rewireModule(MVMComponent, {
    TextBox: React.createClass({ render: function() { return (<div />); }})
});

beforeEach(function () {
    this.instance = TestUtils.renderIntoDocument(<MVMComponent />);
});


it("Instance is defined", function() {
    expect(this.instance).to.exist;
});

});*/

/*describe('Mocking using Sinon', function() {
//var sinon = require('sinon'),
//sandbox = sinon.sandbox.create();


before('before creating textbox', function() {
    TextBox = sinon.stub().returns({
      "<div/>"
    });

    MVMTestComp = proxyquire(process.cwd() + '../js/components/MVM/MVM', {
      './../common/TextBox': reactStub,

    });

    this.MVMCOMP = TestUtils.renderIntoDocument(<MVMTestComp />);
});


it('Component Renders with the Correct DOM', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.MVMCOMP);
    expect(renderedDOM.tagName).to.equal("DIV");
    //expect(renderedDOM.classList.length).to.equal(0);
    //var children = renderedDOM.querySelectorAll('div');
    //expect(children.length).to.equal(11);
});





//afterEach(function() {
//  _sandbox.restore();
//});
});
*/





