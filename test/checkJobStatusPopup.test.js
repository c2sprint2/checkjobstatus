let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let checkJobStatusPopup = require('../js/components/checkJobStatus/checkJobStatusPopup');
let checkJobStatusAPI = require('../js/api/checkJobStatusAPI');
let Table = require('../js/components/checkJobStatus/Table');  //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('checkJobStatusPopup testcases', function () {
  before('before crating checkJobStatusPopup', function() {
    this.checkJobStatusPopupComp = TestUtils.renderIntoDocument(<checkJobStatusPopup />);

});
it('Renderes component with CHECKJOBSTATUSPOPUP tag', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.checkJobStatusPopupComp);
    expect(renderedDOM.tagName).to.equal("CHECKJOBSTATUSPOPUP");
});

  it('renders a noncomposite component', () => {
  let checkJobStatusPopupComp = TestUtils.renderIntoDocument(<checkJobStatusPopup />);
  expect(TestUtils.isCompositeComponent(checkJobStatusPopupComp)).to.equal(false);
});

  it('does not render a react element', () => {
  let checkJobStatusPopupComp = TestUtils.renderIntoDocument(<checkJobStatusPopup />);
  expect(TestUtils.isElement(checkJobStatusPopupComp)).to.equal(false);
});

it('renders a  checkJobStatusPopup component', () => {
  let checkJobStatusPopupComp = TestUtils.renderIntoDocument(<checkJobStatusPopup />);
  expect(TestUtils.isCompositeComponentWithType (checkJobStatusPopupComp,checkJobStatusPopup)).to.equal(false);
});
  

});