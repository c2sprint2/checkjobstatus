'use strict';
import React from 'react'
import App from './components/App'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import store from './store'
import {addLocaleData, IntlProvider} from 'react-intl';
import frLocaleData from 'react-intl/locale-data/fr';
addLocaleData(frLocaleData);
const translations = {
	'fr' : {
			 "Review_Asset_MetaData": "Métadonnées du produit",			 		 
		  }
};
//const locale =  document.documentElement.getAttribute('lang');
const locale = 'en'
render(
  <IntlProvider locale={locale} messages={translations[locale]}>
  <Provider store={store}>
    <App />
  </Provider></IntlProvider>,
 document.getElementById('comp')
)