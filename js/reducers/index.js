import { combineReducers } from 'redux'
import CheckJobStatusReducers from './CheckJobStatusReducers';


const combineCheckJobStatus = combineReducers({
  	CheckJobStatusReducers
 
});

export default combineCheckJobStatus
