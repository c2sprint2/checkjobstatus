import React from 'react';
import Image from './IconComponent.js';
import  ProgressBar from '../common/ProgressBar';
 var img = '/images/accept.png';
class RenderRow extends React.Component{

constructor(props){
    super(props);
    this.state={
      visible:true,
      display : ''
    };
    this.toggle = this.toggle.bind(this);
}

toggle(type){
	var newState = !this.state.visible;
	console.log("new State : "+newState);
	var name = type.Name;
	console.log("toggle : "+name);
    this.setState({visible: newState});
    this.setState({display: name});
    this.props.parent(newState, name);
  }


getAssets(node, style){
    var assets = node;
    var assetArry = [];
      	for(var key in assets){
     		 assetArry.push(
     		 			<div className="children">
      					<div className="col-13" style={style}>{assets[key].name}</div>
      					<div className="col-13" style={style}>{assets[key].size}</div>
      					<div className="col-13" style={style}><ProgressBar/></div>
      					
      					<div className="col-13" style={style}>
      					
      					<span className="img"><Image src={img}/></span>
      					<span>{assets[key].status}</span>

      					</div>

      					</div>
                    );
    }
    return assetArry;
  }

getChildren(self, assetRow){

	var asset = assetRow.assets;
 	var style = {};
	if(assetRow.Name != this.state.display){
      style.display = "none";
    }else if(assetRow.Name == this.state.display){
    	if(this.state.visible){
    		style.display = "none";
    	}else{
    		style.display = "";
    	}
    }
	return this.getAssets(asset, style);
}


render(){
	var childList;
	self = this;
	var nameElement;
	var test = this.props.myFunc;
	var rows = this.props.rows.map(function(item,index){
	var rowArr = [];
	childList = self.getChildren(self,item);
		for(var row in item){
			
			if(row == 'Name'){
					nameElement = <u><a onClick={self.toggle.bind(self,item)}>{item[row]}</a></u>;
			}else if(row == 'Progress'){
				nameElement = <ProgressBar/>;
			}else if(row == 'Status'){
				nameElement = <div><span className='parentImg'><Image src={img}/></span><span>{item[row]}</span>
							  </div>
			}else{
				if(row != 'assets'){
					nameElement = item[row];
				}else{
					nameElement = '';
				}
			}
			if(nameElement != ''){
				rowArr.push(<div className="col-3">{nameElement}</div>);
			}
		}
		
		rowArr.push(childList);
		return <div className="row">{rowArr}</div>
				
	});

	return(
			<div>
				{rows}
			</div>
		)
}
}
RenderRow.propTypes={
	parent:React.PropTypes.func,
};
module.exports= RenderRow;