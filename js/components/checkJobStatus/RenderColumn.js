import React from 'react';

class RenderColumn extends React.Component{

render(){
	
	var column = this.props.cols.map(function(column,index){
		return <div className='col-3' key={index}><b>{column}</b></div>;
	});	

	return(
		<div className="row">
			{column}
		</div>
		)
}
}

module.exports= RenderColumn;