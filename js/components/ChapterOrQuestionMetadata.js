"use strict";
import React from 'react';
import Label from './common/Label';
import TextBox from './common/TextBox';
import Heading from './common/Heading';

class ChapterOrSectionMetadata extends React.Component{

constructor(props) {
    super(props);
    this.displayName = 'ChapterOrSectionMetadata';
    this.handleTextChange = this.handleTextChange.bind(this);
 }

static propTypes= {
    ChapterOrSectionNumber:React.PropTypes.string,
    ChapterOrSectionTitle: React.PropTypes.string
}
static defaultProps= {
    "ChapterOrSectionNumber":"32",
    "ChapterOrSectionTitle":"The Course Of Empire:Expansion and Conflict in America"
}
componentWillMount() {
        
}
componentDidMount() {
      
}
state= {
                ChapterOrSectionNumber:(this.props.data!==undefined)?this.props.data.ChapterOrSectionNumber:"",
                ChapterOrSectionTitle:(this.props.data!==undefined)?this.props.data.ChapterOrSectionTitle:""

}

handleTextChange(obj) {
    var key = obj.id;
    var value = obj.value;
    var stateObj = this.state;
    stateObj[key]=value;
    this.setState(stateObj);
}

 render() {
        return (
            <div>
                <div>
                     <Heading headingType="h3"  value="Chapter or Section Metadata"/>
                </div>
                <div className="pe-input">
                        <Label for="ChapterOrSectionNumber" text="Chapter or Section Number(optional)"/>
                        <TextBox id="ChapterOrSectionNumber" disabled={true} value ={this.state.ChapterOrSectionNumber} handleChange={this.handleTextChange}/>
                </div >
                < div className="pe-input">
                        <Label for ="ChapterOrSectionTitle" text="Chapter or Section Title"/>
                        <TextBox  id="ChapterOrSectionTitle" value ={this.state.ChapterOrSectionTitle} handleChange={this.handleTextChange}/>
                </div >

            </div>  
                
        )
    }

};


module.exports = ChapterOrSectionMetadata;
