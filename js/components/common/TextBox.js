/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
import React from 'react';

class TextBox extends React.Component{

constructor(props) {
    super(props);
    this.displayName = 'TextBox';
    this.handleChange = this.handleChange.bind(this);
 }

static propTypes= {
    id:React.PropTypes.string,
    value: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    required: React.PropTypes.bool,
    maxLength:React.PropTypes.string,
    autofocus: React.PropTypes.bool,
    
}
static defaultProps ={
      id:'',
      value: '',
      placeholder:'',
      disabled:false,
      required:false,
      maxLength:'300',
      autofocus: false,
}
componentWillMount() {
        
}
componentDidMount() {
    //autofocus -- not sure how it will behave if more than one textbox has autofocus "true"
    if (this.props.autofocus){
      this.refs.input.focus();
    }
    //console.log("PROPS",this.props);
        
}
handleChange(e) {
        if (this.props.handleChange) {
            this.props.handleChange({"id":e.target.id,"value":e.target.value});
        }
}

render() {
      var style = {
          input:{
            width : '500px',
            height: '30px',
            'borderRadius': '3px'
          }
      
          };
    
        return (
            <input id={this.props.id} style={style.input} ref="input" type="text" required={this.props.required} maxLength={this.props.maxLength} value ={this.props.value} placeholder={this.props.placeholder} onChange={this.handleChange}  readOnly={this.props.readOnly} disabled={this.props.disabled}/>
        )
    }

};

module.exports = TextBox;
