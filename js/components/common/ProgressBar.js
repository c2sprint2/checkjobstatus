/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
var React = require('react');
var interval = {};
var ProgressBarComp = React.createClass({

propTypes: {

},
        getInitialState: function () {
            return {
                percentage: 5,
                start: true,
                stop: false
            };
        },
        myFunc:function () {
            //this.setState({percentage: 0});
            return "hello";
        },
        shouldComponentUpdate: function (nextProps, nextState) {
            if (nextState.stop === true) {
                clearInterval(interval);
            }
            return (nextState.stop !== true);
        },
        componentDidMount:function () {
            var _this = this;
//            console.log(_this.refs.foo);
//            alert(_this.refs.foo.myFunc());
            var percentVar = 5;
            interval = setInterval(function () {
                if (percentVar < 100) {
                    percentVar = percentVar + 10;
                    _this.setState({percentage: percentVar + "%"});
                } else {
                    clearInterval(interval);
                }
            }.bind(_this), 1000);
        },
        stopProgress:function () {
            this.setState({stop: true});
        },
        render: function () {
            return (
                    < div id = "show-progress-with-jsx" ref = 'foo' >
                    < div className = "progress" >
                    < div className = "progress-bar"
                    style = {{width:this.state.percentage}} > < /div>
                < /div>
                < /div>
                );
        }
});
        module.exports = ProgressBarComp;