/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
import React from 'react';

var RadioGroup = React.createClass({
    render: function() {   
	       return (
            <div>
                {this.props.options.map((CurrOption, index) =>
                	<td>
                		<input type="radio" name={CurrOption.value} value={CurrOption.value}/><span>{CurrOption.text}</span>
                	</td>
                	)}
            </div>
        	)
        }
    });
 
module.exports = RadioGroup;
