/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React from 'react';
import Label from'./../common/Label';
import TextBox from './../common/TextBox';
import Heading from './../common/Heading';
import SelectBox from './../common/SelectBox';
import TextArea from './../common/TextArea';


class MVMComponent extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'MVMComponent';
    this.handleChange = this.handleChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
}
static propTypes= {
		data:React.PropTypes.object.isRequired
}
static defaultProps= {
			data:{}
}
componentWillMount() {
        
}
componentDidMount() {
		if (this.props.autofocus){
			this.refs.input.focus();
		}      
}
state = {
    	UUID:this.props.data.UUID,
    	AssignmentTitle:this.props.data.AssignmentTitle,
    	QuestionName:this.props.data.QuestionName,
    	ContentType:this.props.data.ContentType,
    	AudienceRoles:this.props.data.AudienceRoles,
    	DifficultyLevel:this.props.data.DifficultyLevel,
    	AlignmentType:this.props.data.AlignmentType,
    	KnowledgeLevel:this.props.data.KnowledgeLevel,
    	Description:this.props.data.Description	,
          enablingObejctive:this.props.data.enablingObejctive,
          AudienceRolesData:this.props.data.AudienceRolesData,
          ContentTypeData:this.props.data.ContentTypeData,
          DifficultyLevelData:this.props.data.DifficultyLevelData,
          KnowledgeLevelData:this.props.data.KnowledgeLevelData,
          AlignmentTypeData:this.props.data.AlignmentTypeData
}
handleChange(obj) {
		var key = obj.id;
		var value = obj.text;
		var stateObj = this.state;
		stateObj[key]=value;
        this.setState(stateObj);
       // alert(JSON.stringify(this.state));
        if (this.props.handleChange) {
            this.props.handleChange(this.state);
        }
}
handleTextChange(obj) {
		var key = obj.id;
		var value = obj.value;
		var stateObj = this.state;
		stateObj[key]=value;
        this.setState(stateObj);

        //alert(JSON.stringify(this.state));
        if (this.props.handleChange) {
            this.props.handleChange(this.state);
        }
}
getstateData(){
      return this.state;
}  
render() {
        var style ={
          spacing :{
            'paddingBottom': '15px'
          }
        };
      

        return (
         
            <div>
                <div>
                        <Heading headingType="h3" className="MinimumViable"  value="Minimum Viable MetaData"/>
                </div>
                <div>
                        <Label for="UUID" text="UUID"/>
                        <TextBox id="UUID" disabled={true} value ={this.state.UUID} handleChange={this.handleTextChange} />
                </div >
                < div>
                        <Label for ="AssignmentTitle" text="AssignmentTitle"/>
                        <TextBox  id="AssignmentTitle" value ={this.state.AssignmentTitle} handleChange={this.handleTextChange}/>
                </div >
                < div>
                         <Label for ="QuestionName" text="Question Name"/>
                         <TextBox id="QuestionName" value ={this.state.QuestionName} placeholder = "Add a descriptive Name" handleChange={this.handleTextChange}/>
                </div >
                < div style={style.spacing}>
                          <Label for ="ContentType" text="Content Type"/>
                          <SelectBox id="ContentType" defaultValue ={this.state.ContentType} handleChange={this.handleChange} options={this.state.ContentTypeData}>
                          </SelectBox>
                </div>
                <div style={style.spacing}>
                          <Label for ="AudienceRole" text="Audience Role"/>
                          <SelectBox id="AudienceRole" defaultValue ={this.state.AudienceRoles} handleChange={this.handleChange} options={this.state.AudienceRolesData}>
                          </SelectBox>
                </div>
                <div style={style.spacing}>
                         <Label for ="DifficultyLevel" text="Difficulty Level"/>
                         <SelectBox id="DifficultyLevel" defaultValue ={this.state.DifficultyLevel} disabled={true} handleChange={this.handleChange} options={this.state.DifficultyLevelData}>
                         </SelectBox>
                </div>
                <div style={style.spacing}>
                         <Label for ="KnowledgeLevel" text="Knowledge Level"/>
                         <SelectBox id="KnowledgeLevel" defaultValue ={this.state.KnowledgeLevel} autofocus={true} handleChange={this.handleChange} options={this.state.KnowledgeLevelData}>
                         </SelectBox>
                </div>
                <div style={style.spacing}>
                         <Label for ="AlignmentType" text="Alignment Type"/>
                         <SelectBox id="AlignmentType" defaultValue ={this.state.AlignmentType} handleChange={this.handleChange} options={this.state.AlignmentTypeData}>
                         </SelectBox>
                </div>
                <div>
                         <Label for="EnablingObejctive" text="Enabling Obejctive"/>
                         <TextBox id="EnablingObejctive" value ={this.state.enablingObejctive} handleChange={this.handleTextChange} placeholder="Add enabling obejctive URI"/>
                </div>
                <div>
                         <Label for="Description" text="Description"/>
                         <TextArea id="Description"  value={this.state.Description} placeholder = "Description goes here" handleChange={this.handleTextChange}>
                         </TextArea>
                </div>
            </div>
        )
    }

};


module.exports = MVMComponent;
