import React from 'react'

import CheckJobStatus from '../container/CheckJobStatusContainer';

const App = () => (
  <div className="appDiv">
    <CheckJobStatus/>
  </div>
)

export default App
